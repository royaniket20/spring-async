package com.aniket.async.spring.exceptions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.aniket.async.spring.models.CommonException;
import com.aniket.async.spring.models.ErrorType;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
	

	@ExceptionHandler(NoHandlerFoundException .class)
	public CommonException  handleHttpErrors404(HttpServletRequest httpServletRequest, Exception e)
	{
		CommonException commonException = new CommonException();
		commonException.setExceptionDate(new Date());
		commonException.setExceptionMessage(ErrorType.INVALID_URL.getDescription());
		commonException.setExceptionType(ErrorType.INVALID_URL);
		log.info("****Exception*****{}",commonException);
		return commonException;
		
	}
	
	@ExceptionHandler(MissingPathVariableException .class)
	public CommonException  handleHttpErrors(HttpServletRequest httpServletRequest, Exception e)
	{
		CommonException commonException = new CommonException();
		commonException.setExceptionDate(new Date());
		commonException.setExceptionMessage(ErrorType.INVALID_URL.getDescription());
		commonException.setExceptionType(ErrorType.INVALID_URL);
		log.info("****Exception Path *****{}",commonException);
		return commonException;
		
	}
	
}
