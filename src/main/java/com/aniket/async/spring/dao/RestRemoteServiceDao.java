package com.aniket.async.spring.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aniket.async.spring.models.CountryRestModel;
import com.aniket.async.spring.utils.RestUtility;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RestRemoteServiceDao {

	@Autowired
	private RestUtility restUtility;
	
	
	public List<CountryRestModel> getAllCountries() throws InterruptedException, ExecutionException
	{
		List<CountryRestModel> results = new ArrayList<CountryRestModel>();
		List<CountryRestModel> respponse =  restUtility.getRestTemplateResponse();
		
		//Our performance improvement
		List<String> capitals = respponse.stream().map(CountryRestModel::getCapital).collect(Collectors.toList());
		List<CompletableFuture<List<CountryRestModel>>> allFutures = new ArrayList<>();
		for(String capital : capitals)
		{
			log.info("-----Calling capital service with {}",capital);
			CompletableFuture<List<CountryRestModel>> resp = restUtility.getRestTemplateResponseCapital(capital);
			allFutures.add(resp);
			
		}
		
		 CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[0])).join();
		 
		 for (int i = 0; i < allFutures.size(); i++) {
			 results.addAll(allFutures.get(i).get());
	        }

		
		return results;
	}
}
