package com.aniket.async.spring.services.impl;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aniket.async.spring.dao.RestRemoteServiceDao;
import com.aniket.async.spring.models.CountryRestModel;
import com.aniket.async.spring.services.RestRemoteService;

@Service
public class RestRemoteServiceImpl implements RestRemoteService {

	@Autowired
	private RestRemoteServiceDao restDao;
	
	
	@Override
	public List<CountryRestModel> getAllListOfCountries() throws InterruptedException, ExecutionException {
		return restDao.getAllCountries();
	}

}
