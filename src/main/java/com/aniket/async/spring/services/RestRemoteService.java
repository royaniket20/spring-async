package com.aniket.async.spring.services;

import java.util.List;
import java.util.concurrent.ExecutionException;

import com.aniket.async.spring.models.CountryRestModel;

public interface RestRemoteService {
	
	public List<CountryRestModel> getAllListOfCountries() throws InterruptedException, ExecutionException;
	

}
