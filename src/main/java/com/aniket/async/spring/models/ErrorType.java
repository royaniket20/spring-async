package com.aniket.async.spring.models;

public enum ErrorType {

	INVALID_URL("URL is not valid");

	
	
	private final String description;
	    
	  public String getDescription() {
	      return this.description;
	  }
	ErrorType(String description) {
		this.description=description;
	}
}
