package com.aniket.async.spring.models;

import java.util.List;

import lombok.Data;

@Data
public class CountryRestModel {
	
	private String name;
	private List<String> topLevelDomain;
	private String capital;
	private String region;
	private String subregion;
	private Long population;

}
