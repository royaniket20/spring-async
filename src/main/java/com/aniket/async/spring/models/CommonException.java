package com.aniket.async.spring.models;

import java.util.Date;

import lombok.Data;

@Data
public class CommonException {

	private String exceptionMessage;
	private Date exceptionDate;
	private ErrorType exceptionType;
	

}
