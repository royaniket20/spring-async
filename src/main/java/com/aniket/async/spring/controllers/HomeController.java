package com.aniket.async.spring.controllers;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.aniket.async.spring.models.CountryRestModel;
import com.aniket.async.spring.services.RestRemoteService;

@RestController
public class HomeController {

	@Autowired
	private RestRemoteService restRemoteService;
	
	@GetMapping({"/","/health"})
	public String healthCheck()
	{
		return "Service up and running";
	}
	
	@GetMapping("/home/{name}")
	public String nameCheck(@PathVariable String name)
	{
		return "Welcome Home : "+name;
	}
	
	@GetMapping("/countries")
	public ResponseEntity<List<CountryRestModel>> getAllCountries() throws InterruptedException, ExecutionException
	{
		List<CountryRestModel> data = restRemoteService.getAllListOfCountries();
		return ResponseEntity.ok(data);
		
	}
}
