package com.aniket.async.spring.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.aniket.async.spring.models.CountryRestModel;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RestUtility {

	 @Autowired
	   private RestTemplate restTemplate;
	
	public List<CountryRestModel> getRestTemplateResponse() {

           ResponseEntity<List<CountryRestModel>> response = restTemplate
        	       .exchange("https://restcountries.eu/rest/v2/all", HttpMethod.GET, null,  new ParameterizedTypeReference<List<CountryRestModel>>() {
        	 });
           log.info("******RESPONSE*****{}",response);
          
           return response.getBody();
           

}
	
	 @Async
	public CompletableFuture<List<CountryRestModel>> getRestTemplateResponseCapital(String capital) {
		
        try {
        ResponseEntity<List<CountryRestModel>> response = restTemplate
     	       .exchange("https://restcountries.eu/rest/v2/capital/"+capital, HttpMethod.GET, null,  new ParameterizedTypeReference<List<CountryRestModel>>() {
     	 });
        log.info("******RESPONSE*****{}",response);
        return CompletableFuture.completedFuture(response.getBody());
        }catch (Exception e) {
        	 log.info("******RESPONSE ERROR****{}",e.getMessage());
        	 return CompletableFuture.completedFuture(new ArrayList<CountryRestModel>());
		}
      
        

}
}
